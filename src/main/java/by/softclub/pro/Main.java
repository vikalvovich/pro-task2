package by.softclub.pro;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ClassScanner classScanner = new ClassScanner();
        Map<String, Object> container = classScanner.scanPackage("by.softclub.pro");
        container.forEach((s, o) ->
                System.out.println("Class: " + o.getClass().getSimpleName() + ", annotation name: " + s));

    }
}
