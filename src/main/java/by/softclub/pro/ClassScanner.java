package by.softclub.pro;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ClassScanner {

    public Map<String, Object> scanPackage(String basePackage) {
        Map<String, Object> objects = new HashMap<>();
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AnnotationTypeFilter(MyAnnotation.class));
        for(BeanDefinition bd: provider.findCandidateComponents(basePackage)) {
            try {
                Class<?> aClass = Class.forName(bd.getBeanClassName());
                MyAnnotation annotation = aClass.getAnnotation(MyAnnotation.class);
                objects.put(annotation.name(), aClass.getConstructor().newInstance());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return objects;
    }

}
